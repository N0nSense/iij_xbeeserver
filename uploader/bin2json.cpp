#include <vector>
#include <string>
#include <chrono>
#include <iostream>
#include <iomanip>

//#include  <filesystem>
#include <experimental/filesystem>

#include <cstdio>
#include <cstdint>

std::string API_KEY;
std::string API_SECRET;

namespace fs=std::experimental::filesystem::v1;
//namespace fs=std::filesystem;

int LoadSave(std::string input_file_name, std::string output_file_name);

int main(){
    FILE *api_key_file = fopen("apikey","r");
    if(api_key_file == NULL){
        printf("api key file open fail\n");
        return -1;
    }

    char buf[256];
    fscanf(api_key_file,"%s",buf);
    API_KEY=buf;
    fscanf(api_key_file,"%s",buf);
    API_SECRET=buf;

    if(API_KEY.empty() || API_SECRET.empty()){
        printf("file read fail\n");
        return -1;
    }
    printf("api key file read compleate\n");

    auto path = fs::current_path();
    printf("path:%s\n",path.c_str());
    fs::current_path("../tmp/");
    path = fs::current_path();
    printf("path:%s\n",path.c_str());
    for (const fs::directory_entry& x : fs::directory_iterator("dir")) {
        std::cout << x.path() << " : " << x.is_regular_file() << std::endl;
    }
    //LoadSave("0013A20040AE1A5D_0000016DB43ADCCB","tmp.json");

    return 0;
}


int LoadSave(std::string input_file_name, std::string output_file_name){
    FILE *ifp;
    FILE *ofp;

    printf("\n----- func: LoadSave() -----\n");

    ifp=fopen(input_file_name.c_str(),"rb");
    if(ifp==NULL){
        printf("input file open fail: %s\n",input_file_name.c_str());
        return -1;
    }

    ofp=fopen(output_file_name.c_str(),"w");
    if(ofp==NULL){
        fclose(ofp);
        printf("output file open fail: %s\n",output_file_name.c_str());
        return -1;
    }

    printf("File name:  %s\n",input_file_name.c_str());
    uint64_t time;
    int idx=input_file_name.find_first_of("_");
    std::string id=input_file_name.substr(0,idx);
    printf("ID:         %s\n",id.c_str());
    std::string time_s=input_file_name.substr(idx+1,input_file_name.size());
    printf("Time stamp: %s\n",time_s.c_str());
    time=strtoll(time_s.c_str(),NULL,16);

    std::chrono::milliseconds s;
    s=std::chrono::milliseconds(time);
    std::chrono::time_point<std::chrono::system_clock> sp(s);
    std::time_t t = std::chrono::system_clock::to_time_t(sp);
    const tm* lt = std::localtime(&t);
    std::cout << std::put_time(lt,"%c") << std::endl;

    fseek(ifp,0,SEEK_END);
    long size=ftell(ifp);
    fseek(ifp,0,SEEK_SET);

    printf("file size: %lu  mod9: %lu\n",size,size%9);
    fprintf(ofp,"{\n\"API_KEY\":\"%s\",\n",API_KEY.c_str());
    fprintf(ofp,"\"API_SECRET\":\"%s\",\n",API_SECRET.c_str());
    fprintf(ofp,"\"id\":\"%s\",\n",id.c_str());
    fprintf(ofp,"\"sensor\":[{\n");

    char tag[6][3]={"x1","y1","z1","x2","y2","z2"};
    uint8_t b[9];
    uint16_t bb[6];
    for(int i=0;i<6;i++){
        fprintf(ofp,"\"%s\":[\n",tag[i]);
        while(1){
            if(fread(b,1,9,ifp)!=9){
                break;
            }
            bb[0]=(b[0]<<4)     +(b[1]&0xf0>>4);
            bb[1]=(b[1]&0x0f<<8)+ b[2];
            bb[2]=(b[3]<<4)     +(b[4]&0xf0>>4);
            bb[3]=(b[4]&0x0f<<8)+ b[5];
            bb[4]=(b[6]<<4)     +(b[7]&0xf0>>4);
            bb[5]=(b[7]&0x0f<<8)+ b[8];
            if(ftell(ifp)>size-9){
                if(i==5){
                    fprintf(ofp,"%f\n]\n",float(bb[i])*(2.048f/4096.f));
                    break;
                }
                fprintf(ofp,"%f\n],\n",float(bb[i])*(2.048f/4096.f));
                fseek(ifp,0,SEEK_SET);
                break;
            }
            fprintf(ofp,"%f,",float(bb[i])*(1.f/4096.f));
        }
    }
    fprintf(ofp,"}\n]}");
    fclose(ifp);
    fclose(ofp);
    return 0;

}
