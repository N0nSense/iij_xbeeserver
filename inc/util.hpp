#ifndef UTIL_HPP_
#define UTIL_HPP_

namespace util{
    void BlackChar();
    void RedChar();
    void GreenChar();
    void YellowChar();
    void BlueChar();
    void MagentaChar();
    void CyanChar();
    void WhiteChar();
    void ResetChar();

    void BlackBack();
    void RedBack();
    void GreenBack();
    void YellowBack();
    void BlueBack();
    void MagentaBack();
    void CyanBack();
    void GrayBack();
    void ResetBack();

    void BoldChar();
    void UnderChar();
    void SwapChar();
    void SetDefault();
}
#endif