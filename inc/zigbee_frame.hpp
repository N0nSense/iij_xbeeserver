#ifndef ZIGBEE_FRAME_HPP_
#define ZIGBEE_FRAME_HPP_

#include <vector>
#include <string>
#include <cstdint>
#include <cstdio>

enum ZigBeePacketType{
    kTransmitStatus     = 0x8B,
    kReceivePacket      = 0x90,
    kNodeIdentification = 0x95
};

class ZigBeeFrame{
private:
    uint8_t recieved_checksum;
    uint8_t calculated_checksum;

protected:
    uint8_t checksum;
    uint16_t length;
    uint8_t frame_type;

public:
    // raw data
    std::vector<uint8_t> frame_data;
    std::vector<uint8_t> application_data;

    ZigBeeFrame();

    // 生データからフレームにパースする
    int ImportFrameData(const std::vector<uint8_t> raw_data);
    
    // ﾌﾚｰﾑﾀｲﾌﾟ〜ｱﾌﾟﾘｹｰｼｮﾝﾃﾞｰﾀまでを渡してフレーム形式を作る
    int CreateFrame(const std::vector<uint8_t> application_data);

    // 
    uint8_t CalculateChecksum(const std::vector<uint8_t> data) const;
    void CalculateChecksum();
    int ValidateChecksum();

    uint8_t FrameType() const;

    virtual void PrintStatus() const {}

    int Clear();

};

class ZigBeeNodeIdentification final : public ZigBeeFrame{
public:
    uint16_t source_address_16;
    uint64_t source_address_64;
    uint8_t device_type;

    ZigBeeNodeIdentification(const std::vector<uint8_t> raw_data);
    ZigBeeNodeIdentification(const ZigBeeFrame data);
    ZigBeeNodeIdentification();

    int ImportData(const std::vector<uint8_t> raw_data);
    int ImportData(const ZigBeeFrame data);
    int ImportData();

    void PrintStatus() const override;

    int Clear();
};

class ZigBeeReceivePacket final : public ZigBeeFrame{
public:
    static bool print_ascii;
    uint16_t source_address_16;
    uint64_t source_address_64;
    std::vector<uint8_t> received_data;

    ZigBeeReceivePacket(const std::vector<uint8_t> raw_data);
    ZigBeeReceivePacket(const ZigBeeFrame data);
    ZigBeeReceivePacket();

    int ImportData(const std::vector<uint8_t> raw_data);
    int ImportData(const ZigBeeFrame data);
    int ImportData();

    void PrintStatus() const override;

    int Clear();
};


class ZigBeeTransmitRequest final : public ZigBeeFrame{
public:
    
    int CreateFrame(const std::vector<uint8_t> data, uint64_t dest_addr64, uint16_t dest_addr16=0xFFFE, uint8_t option = 0x00);
    int CreateFrame(const char *data, uint64_t dest_addr64, uint16_t dest_addr16=0xFFFE, uint8_t option = 0x00);

    void PrintStatus() const override;

    int Clear();

};

class ZigBeeTransmitStatus final : public ZigBeeFrame{
public:
    uint16_t dest_addr16;

    uint8_t retry_count;

    uint8_t delivery_status;
    std::string delivery_status_str;

    uint8_t discovery_status;
    std::string discovery_status_str;

    ZigBeeTransmitStatus(const std::vector<uint8_t> raw_data);
    ZigBeeTransmitStatus(const ZigBeeFrame data);
    ZigBeeTransmitStatus();

    int ImportData(const std::vector<uint8_t> raw_data);
    int ImportData(const ZigBeeFrame data);
    int ImportData();

    void PrintStatus() const override;

};

#endif