#ifndef NODE_HPP_
#define NODE_HPP_

#include <vector>

#include <cstdint>
#include <cstdio>

#include "Serial.hpp"
#include "zigbee_frame.hpp"

enum NodeState{
    kDefault,
    kInitialized,
    kReceiving
};

class Node{
private:
    NodeState state;
    uint64_t address;
    uint16_t address16;

    uint16_t dev_payload;

    std::vector<uint16_t> received_list;
    std::vector<uint16_t> unreceived_list;

    int CheckUnreceived();

    FILE *fp;
    int CreateFile();
    int CloseFile();
    int WriteFile(std::vector<uint8_t> data);

public:

    Node();
    int ParseData(ZigBeeFrame data);
    static Serial *serial;
    static bool isSending;
    NodeState GetState();
    uint64_t GetAddress();
    uint16_t GetAddress16();
    int ArrowSend();
};

#endif