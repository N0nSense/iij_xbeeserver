import sys

args = sys.argv
if len(args)!=3:
    print ("usage : "+args[0]+" input_file_path output_file_path")
    exit()


with open(args[1],'rb') as input_file:
    print("input file open: "+args[1])
    with open(args[2],'w') as output_file:
        print("output file open: "+args[2])
        output_file.write("ch1,ch2,ch3,ch4,ch5,ch6\n")
        while True:
            dat=input_file.read(9)
            if dat:
                for i in dat:
                    print("%02X "%i,end='')
                print("\t",end='')
                iidat=[0]*9
                for i in range(9):
                    iidat[i]=int(dat[i]&0xFF)

                idat=[0]*6
                idat[0]=int( ((iidat[0]&0xFF)<<4) | (iidat[1]&0xF0)>>4 )
                idat[1]=int( ((iidat[1]&0x0F)<<8) | (iidat[2]&0xFF)    )
                idat[2]=int( ((iidat[3]&0xFF)<<4) | (iidat[4]&0xF0)>>4 )
                idat[3]=int( ((iidat[4]&0x0F)<<8) | (iidat[5]&0xFF)    )
                idat[4]=int( ((iidat[6]&0xFF)<<4) | (iidat[7]&0xF0)>>4 )
                idat[5]=int( ((iidat[7]&0x0F)<<8) | (iidat[8]&0xFF)    )
                hoge=""
                for i in idat:
                    print("%03X "%i,end='')
                    hoge+=str(i)+","
                hoge=hoge[0:-1]
                output_file.write(hoge+'\n')
                print()
                continue
            else:
                break

print("success")
