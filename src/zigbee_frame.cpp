#include "zigbee_frame.hpp"

#include "util.hpp"

ZigBeeFrame::ZigBeeFrame(){
    this->Clear();
}


int ZigBeeFrame::CreateFrame(std::vector<uint8_t> application_data){
    this->Clear();
    std::copy(application_data.begin(), application_data.end(), std::back_inserter(this->application_data));
    this->length=application_data.size();
    this->frame_type=application_data[0];
    this->CalculateChecksum();
    this->frame_data.reserve(application_data.size()+5);

    this->frame_data.push_back(0x7E);
    this->frame_data.push_back((this->length&0xFF00)>>8);
    this->frame_data.push_back(this->length&0x00FF);
    std::copy(application_data.begin(), application_data.end(), std::back_inserter(this->frame_data));
    this->frame_data.push_back(this->calculated_checksum);
    return 0;
}

int ZigBeeFrame::ImportFrameData(std::vector<uint8_t> raw_data){
    frame_data.clear();
    std::copy(raw_data.begin(), raw_data.end(), std::back_inserter(this->frame_data));
    std::copy(raw_data.begin()+3, raw_data.end()-1, std::back_inserter(this->application_data));
    this->length = (raw_data[1]<<8)+raw_data[2];
    this->frame_type=raw_data[3];
    this->checksum=raw_data.back();
    this->recieved_checksum=raw_data.back();

    return 0;
}

uint8_t ZigBeeFrame::CalculateChecksum(const std::vector<uint8_t> data) const{
    uint8_t sum=0;
    for(auto e: data){
        sum+=e;
    }
    return 0xff-sum;
}

void ZigBeeFrame::CalculateChecksum(){
    this->checksum=CalculateChecksum(this->application_data);
    this->calculated_checksum=this->checksum;
}

int ZigBeeFrame::ValidateChecksum(){
    return (this->recieved_checksum==CalculateChecksum(this->application_data))?0:-1;
}

uint8_t ZigBeeFrame::FrameType() const{
    return this->frame_type;
}


int ZigBeeFrame::Clear(){
    this->checksum=0;
    this->length=0;
    this->frame_type=0;
    this->recieved_checksum=0;
    this->calculated_checksum=0;
    this->frame_data.clear();
    this->application_data.clear();
    return 0;
}

ZigBeeNodeIdentification::ZigBeeNodeIdentification(const std::vector<uint8_t> raw_data){
    ZigBeeNodeIdentification();
    this->ImportData(raw_data);
}

ZigBeeNodeIdentification::ZigBeeNodeIdentification(const ZigBeeFrame data){
    ZigBeeNodeIdentification();
    this->ImportData(data);
}

ZigBeeNodeIdentification::ZigBeeNodeIdentification(){
    this->Clear();
}

int ZigBeeNodeIdentification::ImportData(const std::vector<uint8_t> raw_data){
    this->ImportFrameData(raw_data);
    return this->ImportData();
}

int ZigBeeNodeIdentification::ImportData(const ZigBeeFrame data){
    return this->ImportData(data.frame_data);
}

int ZigBeeNodeIdentification::ImportData(){
    if(this->frame_data.size()==0){
        return -1;
    }
    if(this->frame_type!=0x95){
        return -2;
    }
    this->source_address_64=0;
    for(auto it=this->frame_data.begin()+3; it!=this->frame_data.begin()+12; it++){
        this->source_address_64<<=8;
        this->source_address_64+=*it;
    }
    this->source_address_16=(this->frame_data[12]<<8)+this->frame_data[13];
    this->device_type=this->frame_data[29];

    return 0;
}

void ZigBeeNodeIdentification::PrintStatus() const{
    util::GreenChar();
    util::UnderChar();
    util::BoldChar();
    printf("device joined\n");
    util::SetDefault();
    util::BlueChar();
    printf("\taddr16: ");
    util::SetDefault();
    printf("%X\n",this->source_address_16);
    util::BlueChar();
    printf("\taddr64: ");
    util::SetDefault();
    printf("%016lX\n",this->source_address_64);
    util::BlueChar();
    printf("\ttype:   ");
    util::SetDefault();
    printf("%x\n",this->device_type);
}

int ZigBeeNodeIdentification::Clear(){
    this->ZigBeeFrame::Clear();
    this->source_address_16=0;
    this->source_address_64=0;
    this->device_type=0;
    return 0;
}

bool ZigBeeReceivePacket::print_ascii = true;

ZigBeeReceivePacket::ZigBeeReceivePacket(const std::vector<uint8_t> raw_data){
    ZigBeeReceivePacket();
    this->ImportData(raw_data);
}

ZigBeeReceivePacket::ZigBeeReceivePacket(const ZigBeeFrame data){
    ZigBeeReceivePacket();
    this->ImportData(data);
}

ZigBeeReceivePacket::ZigBeeReceivePacket(){
    this->Clear();
}

int ZigBeeReceivePacket::ImportData(const std::vector<uint8_t> raw_data){
    this->ImportFrameData(raw_data);
    return this->ImportData();
}

int ZigBeeReceivePacket::ImportData(const ZigBeeFrame data){
    return this->ImportData(data.frame_data);
}

int ZigBeeReceivePacket::ImportData(){
        if(this->frame_data.size()==0){
        return -1;
    }
    if(this->frame_type!= kReceivePacket){
        return -2;
    }
    this->source_address_64=0;
    for(auto it=this->frame_data.begin()+3; it!=this->frame_data.begin()+12; it++){
        this->source_address_64<<=8;
        this->source_address_64+=*it;
    }
    this->source_address_16=(this->frame_data[12]<<8)+this->frame_data[13];
    this->received_data.clear();
    std::copy(this->frame_data.begin()+15, this->frame_data.end()-1, std::back_inserter(this->received_data));
    return 0;
}

void ZigBeeReceivePacket::PrintStatus() const {
    util::GreenChar();
    util::UnderChar();
    util::BoldChar();
    printf("packet received\n");
    util::SetDefault();
    util::BlueChar();
    printf("\taddr16: ");
    util::SetDefault();
    printf("%X\n",this->source_address_16);
    util::BlueChar();
    printf("\taddr64: ");
    util::SetDefault();
    printf("%016lX\n",this->source_address_64);
    if(ZigBeeReceivePacket::print_ascii){
        util::BlueChar();
        printf("\tascii : ");
        util::SetDefault();
        for(auto e: this->received_data){
            if(e>=' '&&e<='~'){
                printf("%c",e);
            } else {
                printf("\x1b[34m\\%02X\x1b[39m",e);
            }
        }
        printf("\n");
    }
    util::BlueChar();
    printf("\thex   : ");
    util::SetDefault();
    for(auto e: this->received_data){
        printf("%02X ",e);
    }
    printf("\n");
}

int ZigBeeReceivePacket::Clear(){
    this->ZigBeeFrame::Clear();
    this->source_address_16=0;
    this->source_address_64=0;
    this->received_data.clear();
    return 0;
}


int ZigBeeTransmitRequest::CreateFrame(const std::vector<uint8_t> data, uint64_t dest_addr64, uint16_t dest_addr16, uint8_t option){
    this->Clear();
    this->application_data.push_back(0x10);
    this->application_data.push_back(0x01);
    for(int i=7;i>=0;i--){
        this->application_data.push_back( ((dest_addr64& (0xFFL<<(i*8)) ) >>(i*8))&0xFFL );
    }
    this->application_data.push_back((dest_addr16&0xFF00)>>8);
    this->application_data.push_back(dest_addr16&0x00FF);
    this->application_data.push_back(0x00);
    this->application_data.push_back(option);
    std::copy(data.begin(),data.end(),std::back_inserter(this->application_data));
    return this->ZigBeeFrame::CreateFrame(this->application_data);
}

int ZigBeeTransmitRequest::CreateFrame(const char *str, uint64_t dest_addr64, uint16_t dest_addr16, uint8_t option){
    std::vector<uint8_t> arg;
    while(*str!=0){
        arg.push_back(*str);
        str++;
    }
    return this->CreateFrame(arg,dest_addr64,dest_addr16,option);
}

void ZigBeeTransmitRequest::PrintStatus() const {}

int ZigBeeTransmitRequest::Clear(){
    return this->ZigBeeFrame::Clear();
}



ZigBeeTransmitStatus::ZigBeeTransmitStatus(const std::vector<uint8_t> raw_data){
    ZigBeeTransmitStatus();
    this->ImportData(raw_data);
}

ZigBeeTransmitStatus::ZigBeeTransmitStatus(const ZigBeeFrame data){
    ZigBeeTransmitStatus();
    this->ImportData(data);
}

ZigBeeTransmitStatus::ZigBeeTransmitStatus(){
    this->Clear();
}

int ZigBeeTransmitStatus::ImportData(const std::vector<uint8_t> raw_data){
    this->ImportFrameData(raw_data);
    return this->ImportData();
}

int ZigBeeTransmitStatus::ImportData(const ZigBeeFrame data){
    return this->ImportData(data.frame_data);
}

int ZigBeeTransmitStatus::ImportData(){
    if(this->frame_data.size()==0){
        return -1;
    }
    if(this->frame_type!= kTransmitStatus){
        return -2;
    }
    this->dest_addr16=(this->frame_data[5]<<8)+this->frame_data[6];
    this->retry_count=this->frame_data[7];
    this->delivery_status=this->frame_data[8];
    switch(this->delivery_status){
    case 0x00:
        this->delivery_status_str="Success";
        break;
    case 0x01:
        this->delivery_status_str="MAC ACK Failure";
        break;
    case 0x02:
        this->delivery_status_str="CCA Failure";
        break;
    case 0x15:
        this->delivery_status_str="Invalid destination endpoint";
        break;
    case 0x21:
        this->delivery_status_str="Network ACK Failure";
        break;
    case 0x22:
        this->delivery_status_str="Not Joined to Network";
        break;
    case 0x23:
        this->delivery_status_str="Self-addressed";
        break;
    case 0x24:
        this->delivery_status_str="Address Not Found";
        break;
    case 0x25:
        this->delivery_status_str="Route Not Found";
        break;
    case 0x26:
        this->delivery_status_str="Broadcast source failed to hear a neighbor relay the message";
        break;
    case 0x2B:
        this->delivery_status_str="Invalid binding table index";
        break;
    case 0x2C:
        this->delivery_status_str="Resource error lack of free buffers, timers, etc.";
        break;
    case 0x2D:
        this->delivery_status_str="Attempted broadcast with APS transmission";
        break;
    case 0x2E:
        this->delivery_status_str="Attempted unicast with APS transmission, but EE=0";
        break;
    case 0x32:
        this->delivery_status_str="Resource error lack of free buffers, timers, etc.";
        break;
    case 0x74:
        this->delivery_status_str="Data payload too large";
        break;
    case 0x75:
        this->delivery_status_str="Indirect message unrequested";
        break;
    }

    this->discovery_status=this->frame_data[9];
    switch(this->discovery_status){
    case 0x00:
        this->discovery_status_str="No Discovery Overhead";
        break;
    case 0x01:
        this->discovery_status_str="Address Discovery";
        break;
    case 0x02:
        this->discovery_status_str="Route Discovery";
        break;
    case 0x03:
        this->discovery_status_str="Address and Route";
        break;
    case 0x40:
        this->discovery_status_str="Extended Timeout Discovery";
        break;
    }

    return 0;
}

void ZigBeeTransmitStatus::PrintStatus() const {
    util::GreenChar();
    util::UnderChar();
    util::BoldChar();
    printf("transmit status\n");
    util::SetDefault();
    util::BlueChar();
    printf("\tdest addr16      : ");
    util::SetDefault();
    printf("%04X\n",this->dest_addr16);
    util::BlueChar();
    printf("\tdelivery status  : ");
    util::SetDefault();
    printf("%02X %s\n",this->delivery_status ,this->delivery_status_str.c_str());
    util::BlueChar();
    printf("\tdiscovery status : ");
    util::SetDefault();
    printf("%02X %s\n",this->discovery_status,this->discovery_status_str.c_str());
}
