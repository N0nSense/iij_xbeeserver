#include <iostream>
#include <vector>
#include <queue>
#include <unordered_map>
#include <algorithm>
#include <iterator>
#include <chrono>
#include <iomanip>

#include <cstdio>
#include <cstdint>
#include <ctime>

#include "Serial.hpp"
#include "zigbee_frame.hpp"
#include "node.hpp"
#include "util.hpp"

void PrintCurrentTime(){
    using std::chrono::seconds;
    using std::chrono::milliseconds;
    using std::chrono::system_clock;
    static system_clock::time_point p;
    p=system_clock::now();
    milliseconds s= std::chrono::duration_cast<milliseconds>(p.time_since_epoch());
    //printf("%lX\t%lu\n",s.count(),s.count());
    std::time_t t = system_clock::to_time_t(p);
    const tm* lt = std::localtime(&t);
    util::CyanChar();
    std::cout << std::put_time(lt, "%c") << std::endl;
    util::SetDefault();
}

int main(int argc, char* argv[]){

    if(argc == 1){
        std::cout << "usage : "<< argv[0] <<" [device path]" << std::endl;
        return -1;
    }

    Serial serial;
    Node::serial = &serial;

    if(!serial.open(argv[1],115200U)){
        std::cout << "could not open port." << std::endl;
        return -1;
    }
    std::cout << "port open." << std::endl;

    std::vector<unsigned char> rcv;

    uint16_t length=0;

    std::queue<uint64_t> node_queue;

    ZigBeeReceivePacket::print_ascii=false;

    ZigBeeFrame foo;

    uint8_t rcv_buf;

    int rcv_flag=-1;

    std::unordered_map<uint64_t,Node> node_list;

    while(1){
        rcv_buf=serial.read1byte();

        if(rcv_buf==0x7E){
            rcv.clear();
            rcv.push_back(rcv_buf);
            rcv_flag=0;
            continue;
        }
        
        
        if(rcv_buf==0x7D){
            rcv_buf=serial.read1byte();
            rcv_buf^=0x20;
        }
        rcv.push_back(rcv_buf);

        switch(rcv_flag){
        case 0:
            length=(rcv_buf<<8);
            rcv_flag=1;
            break;
        case 1:
            length+=rcv_buf;
            rcv_flag=2;
            break;
        case 2:
            if(0!=length--){
                break;
            }
            rcv_flag=-1;

            PrintCurrentTime();

            uint64_t address64=0;
            foo.ImportFrameData(rcv);

            if(foo.FrameType() == kNodeIdentification){
                ZigBeeNodeIdentification hogehoge(foo);
                address64 = hogehoge.source_address_64;
                hogehoge.PrintStatus();
            }
            if(foo.FrameType() == kReceivePacket){
                ZigBeeReceivePacket hogehoge(foo);
                address64 = hogehoge.source_address_64;
                hogehoge.PrintStatus();
            }
            if(foo.FrameType() == kTransmitStatus){
                ZigBeeTransmitStatus hogehoge(foo);
                hogehoge.PrintStatus();
            }

            if(address64 != 0){
            
                node_list[address64].ParseData(foo);
                if(node_list[address64].GetState() == kInitialized){
                    node_queue.push(address64);
                    printf("\n");
                    util::GreenBack();
                    util::BlackChar();
                    printf("queue push: %08lX",address64);
                    util::SetDefault();
                    printf("\n");
                }
                /*
                if ( auto iter = node_list.find(address64); iter != end(node_list) ) {
                    node_list[address64].ParseData(foo);
                } else {
                    std::cout << "not exists" << std::endl;
                }
                */
            }

            if(!node_queue.empty() && !Node::isSending){
                node_list[node_queue.front()].ArrowSend();
                printf("queue pop: %08lX\n",node_queue.front());
                node_queue.pop();
            }

            printf("\n\n");
            foo.Clear();
            break;
        //default:
            //break;
        }
        continue;

    }
    serial.close();
    return 0;
}
