#include "node.hpp"

#include <algorithm>
#include <chrono>
#include "util.hpp"

Serial *Node::serial = nullptr;
bool Node::isSending = false;

Node::Node(){
    this->state=kDefault;
    this->received_list.clear();
    this->unreceived_list.clear();
    this->fp=NULL;
}

int Node::ParseData(ZigBeeFrame data){
    // ノード追加通知
    if(data.FrameType() == kNodeIdentification){
        if(this->state != kDefault)
            return -1;
        ZigBeeNodeIdentification tmp = ZigBeeNodeIdentification(data);
        this->address = tmp.source_address_64;
        this->address16 = tmp.source_address_16;
        this->state = kDefault;
        return 0;
    }

    // 
    if(data.FrameType() == kReceivePacket){
        ZigBeeReceivePacket tmp = ZigBeeReceivePacket(data);
        if(this->state == kReceiving){
            uint16_t payload=tmp.received_data[1];
            payload+=uint16_t(tmp.received_data[0])<<8;

            // 受信終了通知を受け取った
            if(payload==0xFFF0){
                util::BlueBack();
                util::WhiteChar();
                printf("receive finish");
                util::SetDefault();
                printf("\n");

                this->CheckUnreceived();
                if(this->unreceived_list.size()){
                    util::RedChar();
                    printf("unreceived list:\n");
                    for(auto e:this->unreceived_list){
                        printf("%04X\n",e);
                    }
                    util::SetDefault();
                }
                this->CloseFile();
                Node::isSending = false;
                this->state = kDefault;
                return 0;
            }

            // 受信開始要求を受け取った
            if(payload == 0xFFFE){
                // TODO: restart
                this->received_list.clear();
                this->unreceived_list.clear();
                this->CloseFile();
                this->CreateFile();
            }

            // パケットのインデックス処理
            if(payload>=0 || payload<=this->dev_payload){
                this->received_list.push_back(payload);
                printf("index: %04X\n",payload);

                std::vector<uint8_t> tmp_data;
                std::copy(tmp.received_data.begin(), tmp.received_data.end(), std::back_inserter(tmp_data));
                tmp_data.erase(tmp_data.begin(),tmp_data.begin()+2);
                this->WriteFile(tmp_data);
            } else {
                util::WhiteChar();
                util::RedBack();
                util::BoldChar();
                printf("invalid index");
                util::SetDefault();
                printf("\n");
            }

        }

        // Identificationがなかったとき用
        if(this->state == kDefault){
            
            this->address = tmp.source_address_64;
            this->address16 = tmp.source_address_16;
            if(tmp.received_data[0]==0xFF && tmp.received_data[1]==0xFE){
                this->state = kInitialized;
                this->dev_payload = tmp.received_data[3];
                this->dev_payload += uint16_t(tmp.received_data[2])<<8;
            } else {
                return -2;
            }
        }

        // アドレス登録済み
        // 送信許可
        if(this->state == kInitialized){
            //this->ArrowSend();
        }
        return 0;
    }
    return 0;
}

NodeState Node::GetState(){
    return this->state;
}

uint64_t Node::GetAddress(){
    return this->address;
}

uint16_t Node::GetAddress16(){
    return this->address16;
}

int Node::ArrowSend(){
    if(Node::isSending){
        util::RedChar();
        util::BoldChar();
        printf("Node is sending, request denied");
        util::SetDefault();
        printf("\n");
        //return -1;
    }
    this->received_list.clear();
    util::BlueBack();
    util::BlackChar();
    printf("send arrow adr: %016llX",this->address);
    util::SetDefault();
    printf("\n");
    this->CreateFile();
    ZigBeeTransmitRequest tmp;
    tmp.CreateFrame({0xFF,0xFE,0x00,0x00},this->address);
    Node::serial->write(tmp.frame_data);
    Node::isSending = true;
    this->state = kReceiving;
    return 0;
}

int Node::CheckUnreceived(){
    std::sort(this->received_list.begin(),this->received_list.end());
    this->unreceived_list.clear();
    uint16_t cnt=0;
    for(auto e:this->received_list){
        while(1) {
            if(cnt!=e){
                this->unreceived_list.push_back(cnt);
                cnt++;
                continue;
            }
            cnt++;
            break;
        }
    }
    if(cnt < this->dev_payload){
        for(uint16_t i=cnt;i<this->dev_payload;i++){
            this->unreceived_list.push_back(i);
        }
    }
    return 0;
}

int Node::CreateFile(){
    using std::chrono::milliseconds;
    using std::chrono::system_clock;
    milliseconds s= std::chrono::duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    uint64_t time=s.count();
    char filename[50];
    do {
        sprintf(filename,"./tmp/%016llX_%016llX",this->address,time);
        this->fp=fopen(filename,"rb");
        if(this->fp != NULL){
            fclose(this->fp);
            time++;
            continue;
        }
    } while(0);
    this->fp=fopen(filename,"wb");
    util::GreenBack();
    util::BlackChar();
    printf("file created: %s",filename);
    util::SetDefault();
    printf("\n");
    return 0;
}

int Node::CloseFile(){
    fclose(this->fp);
    this->fp=NULL;
    util::YellowBack();
    util::BlackChar();
    printf("file closed");
    util::SetDefault();
    printf("\n");
    return 0;
}

int Node::WriteFile(std::vector<uint8_t> data){
    if(this->fp == NULL){
        return -1;
    }
    fwrite(data.data(),1,data.size(),this->fp);
    return 0;
}