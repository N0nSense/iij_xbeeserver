#include <cstdio>

namespace util{
    void BlackChar(){
        printf("\x1b[30m");
    }
    void RedChar(){
        printf("\x1b[31m");
    }
    void GreenChar(){
        printf("\x1b[32m");
    }
    void YellowChar(){
        printf("\x1b[33m");
    }
    void BlueChar(){
        printf("\x1b[34m");
    }
    void MagentaChar(){
        printf("\x1b[35m");
    }
    void CyanChar(){
        printf("\x1b[36m");
    }
    void WhiteChar(){
        printf("\x1b[37m");
    }
    void ResetChar(){
        printf("\x1b[39m");
    }

    void BlackBack(){
        printf("\x1b[40m");
    }
    void RedBack(){
        printf("\x1b[41m");
    }
    void GreenBack(){
        printf("\x1b[42m");
    }
    void YellowBack(){
        printf("\x1b[43m");
    }
    void BlueBack(){
        printf("\x1b[44m");
    }
    void MagentaBack(){
        printf("\x1b[45m");
    }
    void CyanBack(){
        printf("\x1b[46m");
    }
    void GrayBack(){
        printf("\x1b[49m");
    }
    void ResetBack(){
        printf("\x1b[49m");
    }

    void BoldChar(){
        printf("\x1b[1m");
    }
    void UnderChar(){
        printf("\x1b[4m");
    }
    void SwapChar(){
        printf("\x1b[7m");
    }
    void SetDefault(){
        printf("\x1b[0m");
    }
}